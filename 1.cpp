//project euler
//question 1
//R.Selk

#include <iostream>

unsigned long problem1(int x)
{
long sum = 0;
 
  for(int i = 1; i<x; i++)
  {
    if (i%5 == 0)
	{
	  sum += i;
	}
	else if (i%3 == 0)
	{
	  sum += i;
	}
  }
return sum;
}

int main()
{
 //input limit
 std::cout<<problem1(1000);
return 0;
}