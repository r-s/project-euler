//project euler
//question 8
//R.Selk
 
#include <iostream>
#include <fstream>

unsigned int five_consecutive_sum ()
{
  std::ifstream inputfile ("data/8.txt"); 
  
  int array[5] = {0,0,0,0,0};
  int max= 0;
  int sum = 1; //avoid * 0
  int c = 0;
  
  while (inputfile.good())
  {   
    c = (inputfile.get() - '0'); 
		
	for (int i = 0; i<4; ++i)
	  array[i] = array[i+1];
	
	array[4] = c;
	
	for (int i = 0; i<5; ++i)
	  sum *= array[i];
	
	if (sum > max)
	  max = sum;
		
	sum = 1;
   }
  return max;
}


int main()
{
std::cout<<"Largest product of 5 numbers in the stream: "<<five_consecutive_sum()<<std::endl;
return 0;
}

