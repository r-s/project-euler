//project euler
//question 7
//R.Selk

#include <iostream>
#include <cmath> //sqrt

bool check_if_prime(const int number){ //brute force. 
    if(number < 2) 
	return false;
    
	if(number == 2) 
	return true;
	
    if(number % 2 == 0) 
	return false;
    
	for(int i=3; (i*i)<=number; i+=2)
	{
      if(number % i == 0 ) return false;
    }
	
    return true;
}

unsigned int prime_at_position(const int x)
{
  unsigned int count = 0;
  unsigned int i = 1;
  
  while (count != x)
  {
    if (check_if_prime(i) == true)
	{
	  count++;	  
	}
	i++;
  }
return i-1;
}

int main()
{
std::cout<<prime_at_position(10001)<<std::endl;
return 0;
}