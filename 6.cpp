//project euler
//question 6
//R.Selk

#include <iostream>

//This function could be optimized to run in a single loop.
//I left it this way so in future questions I can reference these smaller functions.

unsigned int sum_of_squares (const unsigned int limit)
{
  unsigned int sum = 0; 
  for (unsigned int i = 1; i<= limit; i++)
    sum += i*i;

return sum;
}

unsigned int square_of_sums (const unsigned int limit)
{
  unsigned int sum = 0;
  for (unsigned int i = 1; i<= limit; i++)
  {
    sum += i;
  }
  sum = sum * sum;
}

int main()
{ 
  std::cout<<square_of_sums(100) - sum_of_squares(100)<<std::endl;
return 0;
}
