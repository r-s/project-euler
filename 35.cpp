#include <iostream>
#include <vector>
#include <algorithm>
#include <cmath>


bool check_if_prime(const int number){ //brute force. 
    if(number < 2) 
	return false;
    
	if(number == 2) 
	return true;
	
    if(number % 2 == 0) 
	return false;
    
	for(int i=3; (i*i)<=number; i+=2)
	{
      if(number % i == 0 ) return false;
    }
	
    return true;
}

int shift (int i)
{
  std::vector<int> v; 
  v.erase(v.begin(), v.end());
  while (i>0)
  {
    v.push_back(i%10);
	i /= 10;
  }

int last = *v.begin();
for (std::vector<int>::size_type i = 0; i<v.size()-1; ++i)
{
  v[i] = v[i+1];
}
v[v.size()-1] = last;

//convert to int
  int x = 0, num = 0;
  for (auto i : v)
  {
    num += i * pow(10, x);
	x++;
  }

return num;
}

template <class T>
int numDigits(T number)
{
    int digits = 0;
    if (number < 0) digits = 1; // remove this line if '-' counts as a digit
    while (number) {
        number /= 10;
        digits++;
    }
    return digits;
}

int circularprimes()
{
int count = 0, sum = 0;
  for (int i = 1; i<=1000000; ++i)
  {
    int digits = numDigits(i);
	int x = i;
    for (int j = 0; j<digits; ++j)
	{
	  if (check_if_prime(i))
	  {
	    count++;
		i = shift(i);
	  }
	}
    if (count == digits)
	{
	  sum++;
	}
	i=x;
	count = 0;
  }
return sum;
}



int main()
{
std::cout<<circularprimes();
return 0;
}