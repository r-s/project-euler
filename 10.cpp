//project euler
//question 10
//R.Selk

#include <iostream>

//taken this directly from problem 7 (7.cpp)
bool check_if_prime(int number){ //brute force. 
    if(number < 2) 
	return false;
    
	if(number == 2) 
	return true;
	
    if(number % 2 == 0) 
	return false;
    
	for(int i=3; (i*i)<=number; i+=2)
	{
      if(number % i == 0 ) return false;
    }	
    return true;
}

unsigned long long sum_of_all_primes(long limit)
{
  unsigned long long sum = 0;
  for (unsigned int i=1; i<limit; i++)
  {
   if (check_if_prime(i) == true)
     sum += i;
  }
  return sum;
}

int main()
{
std::cout<<sum_of_all_primes(2000000);
return 0;
}