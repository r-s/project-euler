//project euler
//question 3
//R.Selk

#include <iostream>
#include <cmath> //for sqrt

bool check_if_prime(unsigned long long x)
{
  unsigned long long i = 0;
  
  for (i = 3; i<(sqrt(x)); i+=2) //odd numbers only (even can not be prime). 
    {
	  if (x%i == 0) 
	    return false; //not prime
	}
	
  return true; //prime
}

unsigned long long largest_prime_factor (unsigned long long num)
{
  unsigned long long lpf; // largest prime factor
  
  for (unsigned long long i = 3; i<=num; i+=2) //odd only starting at 3
  {
    if (((num%i) == 0) && check_if_prime(i))
	  {
	    lpf = i;
		std::cout<<lpf<<std::endl; 
	  }
  }
}

int main()
{
  std::cout<<check_if_prime(10)<<std::endl;
  std::cout<<largest_prime_factor(600851475143)<<std::endl; 
  
return 0;
}