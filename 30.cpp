#include <iostream>
//project euler
//question 30
//R.Selk

#include <cmath>
#include <stdlib.h>

bool sumfifthpowers(const int x)
{
  int sum= 0;
  int j = x;
  while (true)
  {
    int p = j % 10;
	j /= 10;
	p = pow(p, 5);
    sum+=p;
	
	if (j<=0)
	  break;
  }
  if (sum == x)
   return true;
  else
   return false;
}


int main()
{
int i = 2;
int sum = 0;

while(true)
{
  if (sumfifthpowers(i))
  {
    sum+= i;
    std::cout<<sum<<std::endl;
  }
  i++;
}

return 0;
}