//project euler
//question 25
//R.Selk


#include <iostream>
#include "BigIntegerLibrary.hh"

BigInteger fibTermValue(const unsigned int x)
{
  unsigned int count = 0;
  BigInteger f1 = 1, f2 = 1, fx = 0;
  
  while (count < x-2)
  {
    fx = f1 + f2; f1 = f2; f2 = fx;
    count++;
  }    
 
return fx;
}

unsigned int countdigits(BigInteger x)
{
  int digits = 0;
  while (x != 0) 
  { 
   x /= 10; 
   digits++; 
  }
  return digits;
}

int main()
{
  BigInteger y;
  int i = 12; //know it cant be <12 from example
  
  do{
      y = fibTermValue(i);
      i++;
    }
  while (countdigits(y) < 1000);
  std::cout<<i-1;
  
return 0;
}
