//project euler
//question 28
//R.Selk

#include <iostream>

int spiralDiagonals (int size)
{
  int count = 0;
  int sum = -3; 
  for (int i=1; i<=(size * size); ++i){
    for (int j = 1; j<=size; j=j+2)
      if (i == (j*j)){      
        sum += i + (i - (count)) + (i-(count+count)) + (i-(count+count+count));
        count +=2;
      }  
    }
return sum;
}

int main()
{
  std::cout<<spiralDiagonals(1001);
return 0;
}  