//project euler
//question 20
//R.Selk

/*
This is the first question using the BigInteger library found on https://mattmccutchen.net/bigint/
May consider writing own BigInt library or adding to this one at a later date -rs
*/

#include <iostream>
#include <cmath>
#include "BigIntegerLibrary.hh"

BigInteger factorial(const unsigned int x)
{
  BigInteger sum = 1; 
  
  for (int i = x; i>0; i--)
  {
    sum*= i;
  }
  return sum;
}

BigInteger sumOfDigits(BigInteger x)
{
  BigInteger sum; //auto initialized to 0
 
  while (x > 1)
  {
    sum += x % 10; 
    x /= 10; 
  }
  
  return sum;
}

int main()
{
  std::cout<<sumOfDigits(factorial(100));
  return 0;
}