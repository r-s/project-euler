//project euler
//question 17
//R.Selk
//I found the main algorithm begining on ln 19 on stackoverflow (Not my code on SO)
//http://stackoverflow.com/questions/4245811/project-euler-problem-17-whats-wrong
#include <iostream>
#include <string>

int lettersintext()
{
  int s[20]= {0,3,3,5,4,4,3,5,5,4,3,6,6,8,8,7,7,9,8,8};
  int d[10]= {0,3,6,6,5,5,5,7,6,6};
  int h = 7; int t =8;
  
  int sum = 0;
  
  for (int i = 0; i<=1000; i++)
  {
    int c = i % 10;
    int b = ((i % 100)-c)/10;
    int a = ((i % 1000) - (b*10) - c) / 100;
    
    if (a != 0)
    {
      sum += s[a] + h;
      if (b!=0 || c != 0)
        sum += 3; //and
    }
    
    if (b == 0 || b == 1)
    {
      sum += s[b * 10 + c];
    }
    else
    {
      sum += d[b] + s[c];
    }
   }
    sum += s[1] + t;
     
      return sum;
}


int main()
{
  std::cout<<lettersintext()<<std::endl;
}