//project euler
//question 48
//R.Selk

#include <iostream>
#include <cmath>
#include "BigIntegerLibrary.hh"
    
BigInteger perfectSquare(const int a)
{
  BigInteger fact = a;
  
  for (int i = 1; i<a; i++)
  {
    fact *= a;
  }
  return fact;
}

BigInteger sumOfSquares(const int a)
{
  BigInteger sum; //auto initialize to 0

  for(int i = 1; i<=a; ++i)
  {
    sum += perfectSquare(i);
  }
  
 return sum;
}

int main()
{
  std::cout<<sumOfSquares(1000)<<std::endl;
  return 0;
}