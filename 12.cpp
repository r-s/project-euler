//project euler
//question 12
//R.Selk

#include <iostream>
#include <cmath>

//another brute force solution.

int count_divisors(unsigned long long num) //tested with 28
{
  unsigned long long count = 0;
  for (unsigned long long i =1; i<=sqrt(num); i++)
    {
	  if ((num % i) == 0)
	    {
		  count++;
		}
	}
	return count * 2;
}

int triangle_number(unsigned long long num) //give triangle number you want
{
  unsigned long long sum = 0;
  for (unsigned long long i =1; i <= num; i++)
    {
	  sum+=i;
	}
	return sum;
}

int triangle_number_divisors (const int num)
{
  unsigned long long  i = 1;
  unsigned long long  trinum = 0;
  unsigned long long  divisors = 0;
  
  while (divisors < num)
    {
	  trinum = triangle_number(i++);  
	  divisors = count_divisors(trinum);  
	}
	return trinum;
}

int main()
{
std::cout<<triangle_number_divisors(500)<<std::endl;
return 0;
}
  
  
  