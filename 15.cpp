//project euler
//question 15
//R.Selk

#include <iostream>
#include <math.h> //need tgamma

//this problem is simple to solve on paper and actually more difficult with code
//since 20! is so large, we cant just use a regular factorial function. 
//by using tgamma we can make the computation much smaller


double factorial(double x)
{
   return tgamma(x+1.);

}
unsigned long long route_through_grid(const int size)
{
return (factorial(size*2)/(factorial(size) * factorial(size)));
}

int main()
{
std::cout<<route_through_grid(20)<<std::endl;
return 0;
}
