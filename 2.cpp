//project euler
//question 2
//R.Selk

#include <iostream>

unsigned long int fib_sum_evens(unsigned int upper_value)
{
  unsigned int current = 1;
  unsigned int previous = 0;
  unsigned int next = 1;
  unsigned long int sum = 0;
  
  while ((current + previous) < upper_value)
  {
    next = current + previous; 
    previous = current; 
    current = next; 
  
  if (next % 2 == 0)
    sum += next; 
  }
return sum;
}


int main()
{
std::cout<<"Sum: "<<fib_sum_evens(4000000);
return 0;
}