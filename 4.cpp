//project euler
//question 4
//R.Selk

#include <iostream>
#include <string>
#include <algorithm> //for std::reverse
#include <sstream> //for stringstring

bool is_palidrome(const unsigned int num) 
{
  //std::string snum = std::to_string(num);
  //For some reason this isn't working on minGW. Known bug, need to update compiler. 
  //use alternative string stream. included sstream in header. 
  
  //int to string so we can get 
  std::stringstream ss;
  ss<<num;
  std::string snum(ss.str());
  std::string snum_reverse = snum;
  
  std::reverse(snum_reverse.begin(), snum_reverse.end());
  
  if (snum.compare(snum_reverse) == 0)
    return true;
  else
    return false;
}

int largest_palidrome()
{
int largest = 0;

  for (int i = 1; i<=999; ++i) //999 = max of 3 digits. 
    for (int j = 1; j<=999; ++j)
	{
	  if (is_palidrome(j*i) == true)
	    {
		  if ((j*i) > largest)
		    largest = (j*i);
		}
	}
return largest;

}

int main()
{
std::cout<<"Largest palidrome of 3 digit multiplication: "<<largest_palidrome()<<std::endl;
return 0;
}