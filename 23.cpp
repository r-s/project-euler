//project euler
//question 23
//R.Selk

#include <iostream>
#include <vector>

bool isabundant(int x)
{
  int sum = 0;
  for(int i = 1; i<x; ++i)
  {
    if (x % i == 0)
      sum+=i;
  }
 return (sum > x) ? 1 : 0;
}

std::vector<int> abundantvec()
{
  std::vector<int> vec;
  
  for (int i = 2; i<=28123; ++i)
  {
    if (isabundant(i))
      vec.push_back(i);
  }
return vec;
}

int sumOfAbundant(std::vector<int> vec)
{
  bool array[28124];

  for (int i = 0; i<vec.size(); i++)
    for (int j = i; j<vec.size(); j++)
      {
        if ((vec[j]+vec[i]) <= 28123)
        {
          array[vec[i] + vec[j]] = 1; 
        }
      }
     
  int sum = 0; 
  for (int x = 0; x<=28123; ++x)
  {
    if (!array[x])
      sum+= x;
  }
return sum;
}
  
int main()
{
 std::cout<<sumOfAbundant(abundantvec());
return 0;
}