//project euler
//question 16
//R.Selk

#include <iostream>
#include <vector>

typedef std::vector<char> numbers; 
void pow2(numbers &in)
{
  int c = 0;
  for (numbers::iterator i = in.begin(); i != in.end(); ++i)
  {
    
	*i = (*i) * 2;
	*i = (*i) + c;
	
	if (*i >=10)
	{
	  c = 1;
	  *i = (*i) - (10);
	}
	else
	  c = 0;
	  	
  }   
  if (c != 0)
    in.push_back(c);
}

int sum(numbers & num)
{
    int runningsum = 0;
    for (numbers::reverse_iterator p = num.rbegin();  p != num.rend();  ++p)
	{
		runningsum += *p;
	}		
	return runningsum;	
}




int main()
{
  numbers num;
  num.push_back(1);
  
  for (int i = 0; i<1000; ++i)
    pow2(num);
  
  std::cout<<sum(num)<<std::endl;
return 0;
}