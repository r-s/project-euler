//project euler
//question 14
//R.Selk

#include <iostream>

//just brute forced it -rs

int collatz(unsigned long long num)
{
unsigned long long length = 1; //count inital 
for (unsigned long long i = 0; num > 1; i++)
  {
  if (num % 2 == 0)
  {
    num = num/2;
  }
  else
  {
    num = (3*num) + 1;
  }
  length++;
  }
  return length;
}

int loop_collatz (unsigned long long limit)
{
  unsigned long long num =0, max = 0, x = 0;
  
  for(unsigned long long i=1; i<limit; ++i)
    {
	  num = collatz(i);
	    if (num > max)
		{
		  max = num;
		  x=i;		
		}
	}
	return x;
}

int main()
{
std::cout<<loop_collatz(1000000)<<std::endl;
return 0;
}