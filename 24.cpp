//project euler
//question 24
//R.Selk

#include <iostream>
#include <string>
#include <algorithm>
#include <sstream>
#include <math.h>
#include <iomanip> 

double lexi_perm(std::string digits, int max)
{
  int myints[10] = {0,0,0,0,0,0,0,0,0,0};
  unsigned int count = 0;
  double ans = 0, power = 0;
  
  for (std::string::size_type x = 0; x<digits.length(); ++x)
   {
     myints[x]= (digits.at(x)) - 48;
   }
  
  do {
    count++;
      if (count == max)
      {
        for (std::string::size_type i = 0; i<digits.length(); ++i)
        {
         power = pow(10, digits.length()-i);
         if (myints[i] != 0)
         ans += myints[i] * (pow(10, digits.length()-i));
         else
         ans /=10;
        }
      }
    } while (std::next_permutation(myints, myints+digits.length()));
   
return ans;
}


int main()
{
std::cout.setf(std::ios::fixed);
std::cout<<std::setprecision(0)<<lexi_perm("0123456789", 1000000);
return 0;
}

  