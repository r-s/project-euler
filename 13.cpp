//project euler
//question 13
//R.Selk

#include <iostream>
#include <string>
#include <fstream>
#include "BigIntegerLibrary.hh"

BigInteger sumOfbignum(std::string filename)
{

  std::string str; std::ifstream infile;
  infile.open(filename.c_str());
  BigInteger a,b,c = 1, buff[50];
  
  while (getline(infile,str))
  {    
       for (unsigned int i = 0; i<str.length(); ++i)
       {
         buff[i] = (str[i]) - 48;
       }        

       for (unsigned int i = 0; i <str.length(); ++i)
       {
         a += (buff[49-i]) * c;
         c *= 10;
       }
       
      c = 1; b += a; a = 0; 
  }
return b;
}

int main()
{
std::cout<<sumOfbignum("nums.txt")<<std::endl;
return 0;
}