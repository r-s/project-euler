//project euler
//question 22
//R.Selk

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <algorithm>
#include <sstream>

unsigned int numOfChar(char x)
{
  int y = (int)x;
  
  if (y >= 65 && y<=90)
  {
    return (y-64);
  }
  else if (y>=97 && y<= 122)
  {  
    return (y-96);
  }
  else
    return 0;
}

unsigned int valueOfString(std::string x, int p)
{
  unsigned int sum = 0;
  for (std::string::size_type i = 0; i<=x.length(); ++i)
  {
    sum += numOfChar(x[i]);
  }
  sum *= (p+1);
  
  return sum;
}    
//from : http://stackoverflow.com/questions/1120140/csv-parser-in-c
std::vector<std::string> getNextLineAndSplitIntoTokens(std::istream& str)
{
  std::vector<std::string>   result;
  std::string                line;
  std::getline(str,line);

  std::stringstream          lineStream(line);
  std::string                cell;

  while(std::getline(lineStream,cell,','))
  {
    result.push_back(cell);
  }
  return result;
}

unsigned long long sumOfAll(std::string input)
{
  std::fstream file (input.c_str());
  std::vector<std::string> vec;
  vec = getNextLineAndSplitIntoTokens(file);
  
  unsigned long long sum = 0;
  std::sort(vec.begin(), vec.end()); 

  for (std::vector<std::string>::size_type p = 0; p<vec.size(); ++p)
  {
    sum += valueOfString(vec[p], p);
  }
  return sum;
}

int main()
{
  std::cout<<sumOfAll("names.txt");
  return 0;
}

