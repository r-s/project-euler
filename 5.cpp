//project euler
//question 5
//R.Selk

#include <iostream>

int smallest_evenly_divisible (const unsigned int limit)
{
  unsigned int count = 1;
  unsigned int x = 1; 
  
  while (x!= 0)
  {
    x = 0;
	for (unsigned int i=1; i<=limit; i++)
	{
	  x+=(count % i);
	}
	count++;
  }
  
  return count-1;
}

int main()
{
std::cout<<smallest_evenly_divisible(20)<<std::endl;
return 0;
}