//project euler
//question 9
//R.Selk

#include <iostream>

bool is_pythag_triplet (int a, int b, int c)
{
  if ((a > b) || (b > c))
    return false;
  if ((a*a) + (b*b) == (c*c))
	return true;
  else
    return false;
}

bool sum_1000(int a, int b, int c)
{
  if (a+b+c == 1000)
  return true;
  else
  return false;
}

int product()
{
for (int i = 1; i<998; i++) //this is approx a trillion operations, worst case but very unlikely it will run anywhere near that amount. Could be optimized. 
 for (int j = 1; j<998; j++)
  for (int y = 1; y<998; y++)
  {
    if ((is_pythag_triplet(i,j,y) == true) && (sum_1000(i,j,y) == true))
	{
	return (i*j*y);
	}
  }
}

int main()
{
std::cout<<product();
return 0;
}